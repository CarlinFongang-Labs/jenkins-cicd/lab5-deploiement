# Jenkins | Deployment

[Please find the specifications by clicking](https://github.com/eazytraining/)

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Prerequisites:
1. Create an account on [Heroku](https://heroku.com).
2. Retrieve the API key.
3. Create a secret on Jenkins named `HEROKU_API_KEY`.
4. Setting [Build & Artifact stage](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab4-artefact)

## Context:
1. Create a pipeline job named "deployment."
2. Ensure that the "GitHub Integration" plugin is installed and configure the webhook on the GitHub project to automate the build executed by Jenkins.
3. Configure the pipeline to be triggered by any changes made to the GitHub project repository (configure the build trigger accordingly).
4. Create a Jenkinsfile in the repository with the following steps:
  - Building the Docker image (agent none)
  - Creating a container from this image (agent none)
  - Testing the image using the curl command (agent none)
  - Using the Heroku CLI (to be installed) to build, push, and deploy the image to Heroku, only on the master/main branch (agent none) [Heroku documentation](https://devcenter.heroku.com/articles/container-registry-and-runtime)
  - Deployment on Heroku will initially be in a staging environment `<user>-staging`, then in a production environment `<user>-production`
  - Note: parameterize the project names


## Creating a Heroku Account

1. [Refer to the procedure for creating a Heroku account](https://gitlab.com/CarlinFongang-Labs/gitlab-cicd/lab4-deployment#cr%C3%A9ation-du-compte-heroku)

2. Retrieve the API key in the account settings.
>![alt text](img/image.png)
*API Key on heroku account*

### Creation of the Jenkins identifier.
Adding a Heroku credential to Jenkins will allow the latter to authenticate during the deployment job.
Dashboard > Jenkins Administration > Security > Credentials > Add Credentials
>![alt text](img/image-1.png)
*Add New Credentials Interface*

>![alt text](img/image-2.png)
*Entering the Heroku API Key*

### Configuring GitHub Integration with Jenkins
Administer Jenkins > Plugins > Available Plugins > "Search for... : Github Integration"
>![alt text](img/image-3.png)
*Search for the "Github Integration" plugin*

>![alt text](img/image-4.png)
*Successful plugin installation*


### Configuring GitHub Webhook
Navigate to the project repository, in our case it's: https://github.com/CarlinFongang/alpinehelloworld
go to the project settings
Setting > Webhooks > Add webhook
>![alt text](img/image-5.png)

- Adding information to the webhook parameters
    - Payload URL: `http://<url_jenkins>/github-webhook/`
    - Content type: `application/json`

Disclaimer: As this is an ephemeral Jenkins server, we will not configure a secret, but it is highly recommended to configure a secret for security reasons in the case of permanent projects.

 >![alt text](img/image-6.png)
*GitHub Webhook Configuration*

>![alt text](img/image-7.png)
*Successful Jenkins Connection Verification*

Once the webhook is created and verified, click on it to view the console output of the verification test performed by GitHub.

>![alt text](img/image-8.png)
*Console output of the webhook test to Jenkins*

>![alt text](img/image-9.png)
*Response from the GitHub request to Jenkins*

### Creation of the Jenkinsfile
1. In this Jenkinsfile, we have defined various variables to be used in the stages as well as the status of the agent to be used.

````
pipeline {
    environment {
        IMAGE_NAME = "alpinehelloworld"
        IMAGE_TAG = "latest"
        STAGING = "carlinfg-staging"
        PRODUCTION = "carlinfg-production"
        IP_ADDRESS = "44.222.148.16"
    }
````

2. We have also defined the different stages to be executed in our pipeline.
`Build image`, `Run container based on builded image`, `Test image`, `Clean test`, `Push image in staging and deploy it`, `Push image in production and deploy it`

3. Adding the Jenkinsfile to the project
Next, we will add this Jenkinsfile to the GitHub project: [Project here](https://github.com/CarlinFongang/alpinehelloworld)

Dashbord > Add New File
> ![alt text](img/image-10.png)
*Adding a new file to the alpinehelloworld project*

> ![alt text](img/image-11.png)
*Committing the new Jenkinsfile*

### Creating the Jenkins Job for Dynamic Code Retrieval from GitHub

Dashboard > New Item
> ![alt text](img/image-12.png)
*Creating the alpinehelloworld pipeline*

1. Enter the project's repository.
> ![alt text](img/image-13.png)

2. Enable the GitHub hook trigger function.
This will trigger the pipeline upon receiving a GitHub push hook.
> ![alt text](img/image-14.png)

3. Define the source script necessary for building the pipeline.
> ![alt text](img/image-15.png)
*Defining the repository containing the various stages of the pipeline to execute*

4. Specify the "master" branch along with the path to the "Jenkinsfile".
> ![alt text](img/image-16.png)

Once this setup is complete, save your configuration.

#### Running the Pipeline
Dashboard > alpinehelloworld > Build Now
> ![alt text](img/image-17.png)

### Publishing Different Environments on Heroku
> ![alt text](img/image-18.png)
*Creating the staging and production environments on Heroku*

> ![alt text](img/image-19.png)
*Staging on Heroku*

> ![alt text](img/image-20.png)
*Application available in staging*

> ![alt text](img/image-21.png)
*Application available in production*



